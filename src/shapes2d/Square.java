package shapes2d;

public class Square {

    public double lengthOfSide;

    public Square(double lengthOfSide) {
        this.lengthOfSide = lengthOfSide;
    }

    public double area(){
        return lengthOfSide*lengthOfSide;
    }

    @Override
    public String toString(){
        return "side: "+lengthOfSide;
    }

}