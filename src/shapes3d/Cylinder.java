package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {

    public double heightOfCylinder;

    public Cylinder(double radius, double heightOfCylinder) {
        super(radius);
        this.heightOfCylinder = heightOfCylinder;
    }


    public double volume(){
        return super.area()*heightOfCylinder;
    }

    public double area(){
        return (2*Math.PI*super.radius*heightOfCylinder)+(super.area()*2);
    }

    @Override
    public String toString(){

        return "radius: "+radius+"  height: "+heightOfCylinder;
    }
}