package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(double lengthOfSide) {
        super(lengthOfSide);
    }


    public double area(){
        return super.area()*6;
    }

    public double Volume()
    {
        return (super.area())*(super.lengthOfSide);
    }

    @Override
    public String toString(){
        return "side: "+lengthOfSide;
    }


}